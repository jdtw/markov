{-# LANGUAGE DeriveDataTypeable #-}

import           Control.Monad.State
import           Data.List
import qualified Data.Map                        as Map
import           System.Console.CmdArgs.Implicit
import           System.Random

type MarkovMap = Map.Map String String
type MarkovState = (StdGen, String)

data Granularity = Word | Character deriving (Show, Data, Typeable)
data MarkovArgs  = MarkovArgs { granularity :: Granularity
                              , stateLength :: Int
                              , nWords      :: Int
                              , file        :: String
                              } deriving (Show, Data, Typeable)

markovArgs :: MarkovArgs
markovArgs = MarkovArgs { granularity = enum [ Character &= help "Use character granularity", Word &= help "Use word granularity"]
                        , stateLength = 2 &= help "How many characters or words (depending on the granularity) to keep as state"
                        , nWords      = 100 &= help "How many words to print"
                        , file        = def &= argPos 0 &= typFile}
             &= summary "Markov v0.1"

transition :: MarkovMap -> State MarkovState Char
transition m = do
    (gen, current) <- get
    let options = m Map.! current
        (index, newGen) = randomR (0, length options - 1) gen
        nextChar = options !! index
    put (newGen, tail current ++ [nextChar])
    return nextChar

transition' :: (Ord a) => Map.Map [a] [a] -> State (StdGen, [a]) a
transition' m = do
  (gen, current) <- get
  let options = m Map.! current
      (index, newGen) = randomR (0, length options - 1) gen
      nextWord = options !! index
  put (newGen, tail current ++ [nextWord])
  return nextWord

getWords :: MarkovMap -> Int -> [String]
getWords m n =
    let keys        = filter ((==) ' ' . last) $ Map.keys m
        (r, gen)    = randomR (0, length keys - 1) $ mkStdGen 137
        startState  = keys !! r
        markovChars = evalState (sequence . repeat $ transition m) (gen, startState)
    in  take n . words $ markovChars

getWords' :: (Ord a) => Map.Map [a] [a] -> [a]
getWords' m =
    let (r, gen) = randomR (0, length (Map.keys m) - 1) $ mkStdGen 137
        startState = (Map.keys m) !! r
        markovWords = evalState (sequence . repeat $ transition' m) (gen, startState)
    in markovWords

printWords :: [String] -> IO ()
printWords = mapM_ putStrLn . makeLines
    where makeLines [] = []
          makeLines xs = unwords (take 10 xs) : makeLines (drop 10 xs)

main :: IO ()
main = do
{-
    MarkovArgs _ stateLen nwords fileName <- cmdArgs markovArgs
    contents <- readFile fileName
    let chainMap = chain stateLen . unwords . words $ contents
    printWords $ getWords chainMap nwords
-}
    MarkovArgs gran stateLen nwords fileName <- cmdArgs markovArgs
    contents <- readFile fileName
    let chainMap = chain' stateLen . unwords . words $ contents
    print $ getWords' chainMap


chain :: Int -> String -> Map.Map String String
chain n xs = Map.fromListWith (++) $ zip from to
    where from = map (take n) . tails $ xs ++ " " ++ xs
          to   = drop n . map (:[]) $ xs ++ " " ++ take n xs

chain' :: (Ord a) => Int -> [a] -> Map.Map [a] [a]
chain' n xs = Map.fromListWith (++) $ zip from to
    where from = map (take n) .tails $ (cycle xs)
          to = drop n . map (:[]) $ xs ++ take n xs